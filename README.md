# Painting Canvas

Android App that serves as a drawing app

Features:
- Color picking
- Brush size selection
- Undo button
- Redo button
- Select image from phone gallery
- Safe drawn image on the phone
- share picture

<table>
    <tr>
        <td>
            <img src="https://gitlab.com/app-development8/android/drawing-canvas/-/raw/main/app/src/main/res/drawable/demo_plain_screen.png"       width="300" height="585"/>
        </td>
        <td>
            <img src="https://gitlab.com/app-development8/android/drawing-canvas/-/raw/main/app/src/main/res/drawable/demo_eifel.png" width="300" height="585"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://gitlab.com/app-development8/android/drawing-canvas/-/raw/main/app/src/main/res/drawable/demo_gitlab.png" width="300" height="585"/>
        </td>
                <td>
            <img src="https://gitlab.com/app-development8/android/drawing-canvas/-/raw/main/app/src/main/res/drawable/demo_share.png" width="300" height="585"/>
        </td>
    </tr>
</table>
